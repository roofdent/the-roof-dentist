The Roof Dentist provides all kinds of roof repairs to residents in Melbourne�s eastern suburbs. From Terra Cotta, chimneys to concrete roof finishes, we also repair and restore all types of tiled roofs. Call us for quality roof repairs today.

Address: 46A Birdwood Street, Box Hill South, VIC  3128, Australia

Phone: +61 3 9899 6651